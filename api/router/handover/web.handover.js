// init dependencies
var express = require('express');
var sql = require('mssql');
var router = express.Router();
var path = require('path');

// moment
var moment = require('moment');
var locale = require('moment/locale/id');
moment.locale('id');


// Docx Templater
var JSZip = require('jszip');
var Docxtemplater = require('docxtemplater');

var fs = require('fs');
var path = require('path');




// call core functions
var core = require('../../multiple.core.js');

// call sql connection configuration
var config = require('../../multiple.config.js');
console.log(config.application() + ' [' + path.basename(__filename) + ']');
console.log(config.setup());


// ------------------------------------------------------------------------------------------------------------------------------------------------ //
// ------------------------------------------------------------------ NOTES ----------------------------------------------------------------------- //
// ------------------------------------------------------------------------------------------------------------------------------------------------ //

/*
    When you are using SQL Server 2014 and you've output parameter. ExpressJS doesn't care about output parameter, so it will be treated as input parameter.
    Please remember, ExpressJS can't return output parameter as a table. So, the behavior between ExpressJS and WebService in C# doesn't have similarity.
    If you need an output as a table, you have to select manually at the end of your store procedure.
*/

// ================================================================== POST ROUTER ================================================================== //

router.get('/handover-letter', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Search", true);
});

router.get('/handover-letter-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.post('/handover-letter-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Purpose", obj[0].Purpose);
    core.AddWithParameter(table1, "HandoverDate", obj[0].HandoverDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "IPLFee", obj[0].IPLFee);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Save", true);
});

router.get('/handover-letter-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Info", true);
});

router.post('/handover-letter-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Purpose", obj[0].Purpose);
    core.AddWithParameter(table1, "HandoverDate", obj[0].HandoverDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "IPLFee", obj[0].IPLFee);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Update", true);
});

router.post('/handover-letter-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Approve", true);
});

router.post('/handover-letter-sent', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Sent", true);
});

// Untuk Download Word
router.get('/handover-letterprint-rumah', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima_rumah.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

// Untuk Download Word
router.get('/handover-letterprint-ruko', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima_ruko.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

// Untuk Download Word
router.get('/handover-letterprint-kavling', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima_kavling.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});


router.get('/dashboard', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_Handover_Dashboard", true);
});



router.get('/construction-progress', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.get('/additional-list', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);
    console.log(obj);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_ConstructionProgress_AdditionalList", true);
});

router.get('/handover-construction', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "ProductID", obj[0].ProductID);
    core.AddWithParameter(table1, "SubCluster", obj[0].SubCluster);
    core.AddWithParameter(table1, "Address", obj[0].Address);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverConstruction_Search", true);
});

router.get('/handover-construction-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_HandoverConstruction_SelectProduct_Search", true);
});

router.get('/handover-construction-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverConstruction", obj[0].IDX_T_HandoverConstruction);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverConstruction_Info", true);
});

router.post('/handover-construction-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "HandoverConstructionDate", obj[0].HandoverConstructionDate);
    core.AddWithParameter(table1, "PIC_CRM", obj[0].PIC_CRM);
    core.AddWithParameter(table1, "PIC_Construction", obj[0].PIC_Construction);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverConstruction_Save", true);
});

router.post('/handover-construction-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverConstruction", obj[0].IDX_T_HandoverConstruction);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverConstruction_Approve", true);
});

//-------------------------- Undangan Pengecekan Rumah ------------------------
router.get('/home-checking-letter', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Search", true);
});

router.get('/home-checking-letter-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.post('/home-checking-letter-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "HomeCheckingDate", obj[0].HomeCheckingDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Save", true);
});

router.get('/home-checking-letter-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HomeCheckingLetter", obj[0].IDX_T_HomeCheckingLetter);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Info", true);
});

router.post('/home-checking-letter-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_HomeCheckingLetter", obj[0].IDX_T_HomeCheckingLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "HomeCheckingDate", obj[0].HomeCheckingDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Update", true);
});

router.post('/home-checking-letter-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HomeCheckingLetter", obj[0].IDX_T_HomeCheckingLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Approve", true);
});

// Untuk Download Word
router.get('/homechecking-letterprint', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HomeCheckingLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HomeCheckingLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./cekrumah.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            HomeCheckingDate: moment(result.HomeCheckingDate).format('LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: moment(result.StartTime).format('LL'),
            EndTime: moment(result.EndTime).format('LL'),
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
        

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});


//-------------------------- Undangan Serah Terima Sepihak ------------------------
router.get('/unilateral-letter', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Search", true);
});

router.get('/unilateral-letter-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.post('/unilateral-letter-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "DateLetterBefore", obj[0].DateLetterBefore);
    core.AddWithParameter(table1, "RetentionPeriod", obj[0].RetentionPeriod);
    core.AddWithParameter(table1, "CountedDate", obj[0].CountedDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Save", true);
});

router.get('/unilateral-letter-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_UnilateralLetter", obj[0].IDX_T_UnilateralLetter);

    console.log(obj);
    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Info", true);
});

router.post('/unilateral-letter-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_UnilateralLetter", obj[0].IDX_T_UnilateralLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "DateLetterBefore", obj[0].DateLetterBefore);
    core.AddWithParameter(table1, "RetentionPeriod", obj[0].RetentionPeriod);
    core.AddWithParameter(table1, "CountedDate", obj[0].CountedDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Update", true);
});

router.post('/unilateral-letter-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_UnilateralLetter", obj[0].IDX_T_UnilateralLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Approve", true);
});

// Untuk Download Word
router.get('/unilateral-letterprint', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_UnilateralLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_UnilateralLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./sepihak.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetterBefore: moment(result.DateLetterBefore).format('LL'),
            RetentionPeriod: result.RetentionPeriod,
            CountedDate: moment(result.CountedDate).format('LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: moment(result.StartTime).format('LL'),
            EndTime: moment(result.EndTime).format('LL'),
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
        

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

//-------------------------- Undangan Retensi Aktif ------------------------------
router.get('/retention-letter', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionLetter_Search", true);
});

router.get('/retention-letter-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.post('/retention-letter-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "RetentionPeriod", obj[0].RetentionPeriod);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionLetter_Save", true);
});

router.get('/retention-letter-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_RetentionLetter", obj[0].IDX_T_RetentionLetter);

    console.log(obj);
    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionLetter_Info", true);
});

router.post('/retention-letter-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_RetentionLetter", obj[0].IDX_T_RetentionLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "RetentionPeriod", obj[0].RetentionPeriod);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionlLetter_Update", true);
});

router.post('/retention-letter-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_RetentionLetter", obj[0].IDX_T_RetentionLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionLetter_Approve", true);
});

// Untuk Download Word
router.get('/retention-letterprint', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_RetentionLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_RetentionLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./retensi.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            RetentionPeriod: result.RetentionPeriod,
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: moment(result.StartTime).format('LL'),
            EndTime: moment(result.EndTime).format('LL'),
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
        

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

//-------------------------- Undangan Pengambilan PBB ------------------------------
router.get('/pbb-letter', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Search", true);
});

router.get('/pbb-letter-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProductPBB_Search", true);
});

router.post('/pbb-letter-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Save", true);
});

router.get('/pbb-letter-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_TakingPBBLetter", obj[0].IDX_T_TakingPBBLetter);

    console.log(obj);
    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Info", true);
});

router.post('/pbb-letter-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_TakingPBBLetter", obj[0].IDX_T_TakingPBBLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Update", true);
});

router.post('/pbb-letter-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_TakingPBBLetter", obj[0].IDX_T_TakingPBBLetter);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Approve", true);
});

// Untuk Download Word
router.get('/pbb-letterprint', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_TakingPBBLetter", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_TakingPBBLetter_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./pbb.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
        

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

//-------------------------- Undangan Serah Terima 2 ------------------------------
router.get('/handover-letter2', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "CustomerName", obj[0].CustomerName);
    core.AddWithParameter(table1, "ProductDesc", obj[0].ProductDesc);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Search", true);
});

router.get('/handover-letter2-form', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "Keyword", obj[0].Keyword);
    core.AddWithParameter(table1, "IDX_M_GroupCompany", obj[0].IDX_M_GroupCompany);
    core.AddWithParameter(table1, "CurrentPage", obj[0].CurrentPage);
    core.AddWithParameter(table1, "PageSize", obj[0].PageSize);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_SelectProduct_Search", true);
});

router.post('/handover-letter2-save', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_M_LetterType", obj[0].IDX_M_LetterType);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Purpose", obj[0].Purpose);
    core.AddWithParameter(table1, "HandoverDate", obj[0].HandoverDate);
    core.AddWithParameter(table1, "DateLetterBefore", obj[0].DateLetterBefore);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "IPLFee", obj[0].IPLFee);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Save", true);
});

router.get('/handover-letter2-info', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.query.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", obj[0].IDX_T_HandoverLetter2);

    console.log(obj);
    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Info", true);
});

router.post('/handover-letter2-edit', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_M_Product", obj[0].IDX_M_Product);
    core.AddWithParameter(table1, "IDX_T_SalesOrderHeader", obj[0].IDX_T_SalesOrderHeader);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", obj[0].IDX_T_TakingPBBLetter);
    core.AddWithParameter(table1, "LetterNo", obj[0].LetterNo);
    core.AddWithParameter(table1, "DateLetter", obj[0].DateLetter);
    core.AddWithParameter(table1, "Subject", obj[0].Subject);
    core.AddWithParameter(table1, "Purpose", obj[0].Purpose);
    core.AddWithParameter(table1, "HandoverDate", obj[0].HandoverDate);
    core.AddWithParameter(table1, "Location", obj[0].Location);
    core.AddWithParameter(table1, "DateLetterBefore", obj[0].DateLetterBefore);
    core.AddWithParameter(table1, "IPLFee", obj[0].IPLFee);
    core.AddWithParameter(table1, "StartTime", obj[0].StartTime);
    core.AddWithParameter(table1, "EndTime", obj[0].EndTime);
    core.AddWithParameter(table1, "Notes", obj[0].Notes);

    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Update", true);
});

router.post('/handover-letter2-approve', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", obj[0].IDX_T_HandoverLetter2);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].ApprovalNotes);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Approve", true);
});

router.post('/handover-letter2-sent', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    let obj = JSON.parse(req.body.id);

    // define parameter
    core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    core.AddWithParameter(table1, "FormID", obj[0].FormID);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", obj[0].IDX_T_HandoverLetter2);
    core.AddWithParameter(table1, "Courier", obj[0].Courier);
    core.AddWithParameter(table1, "ResiNo", obj[0].ResiNo);
    core.AddWithParameter(table1, "SentDate", obj[0].SentDate);
    core.AddWithParameter(table1, "ApprovalNotes", obj[0].SentDate);
    core.AddWithParameter(table1, "FlagSent", obj[0].FlagSent);
    


    //execute query
    return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Sent", true);
});


// Untuk Download Word
router.get('/handover-letterprint2-rumah', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima2_rumah.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            DateLetterBefore: moment(result.DateLetterBefore).format('LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

// Untuk Download Word
router.get('/handover-letterprint2-ruko', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima2_ruko.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});

// Untuk Download Word
router.get('/handover-letterprint2-kavling', function(req, res) {
    // define and prepare table parameter
    const table1 = new sql.Table();
    core.PrepareTableParameter(table1);

    // parse json to object
    //let obj = JSON.parse(req.query.id);

    // define parameter
    // core.AddWithParameter(table1, "SessionID", obj[0].SessionID);
    // core.AddWithParameter(table1, "FormID", obj[0].FormID);
    // core.AddWithParameter(table1, "IDX_T_HandoverLetter", obj[0].IDX_T_HandoverLetter );

    core.AddWithParameter(table1, "SessionID", req.query.sessionid);
    core.AddWithParameter(table1, "FormID", req.query.formid);
    core.AddWithParameter(table1, "IDX_T_HandoverLetter2", req.query.id );

    let result = [];
    //execute query
     core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_HandoverLetter2_Info", true, false).then(output => {
        result = output[3][0];
        console.log(result);
        var content = fs.readFileSync(path.resolve('./serah_terima2_kavling.docx'), 'binary');

        var zip = new JSZip(content);
        var doc = new Docxtemplater();
        doc.loadZip(zip);

        //set the templateVariables
        doc.setData({
            DateLetter: moment(result.DateLetter).format('LL'),
            DateLetter2: moment(result.DateLetter).format('dddd, LL'),
            LetterNo: result.LetterNo,
            Subject: result.Subject,
            CustomerName: result.CustomerName,
            StartTime: result.StartTime,
            EndTime: result.EndTime,
            Purpose: result.Purpose,
            Notes: result.Notes,
            Location: result.Location,
            Address: result.Address,
            IPLFee: result.IPLFee,
            PPN: result.IPLFee * 0.1,
            Total: result.IPLFee +  (result.IPLFee * 0.1),
            Type: result.Type

        });

        try {
            // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
            doc.render()
        }
        catch (error) {
            var e = {
                message: error.message,
                name: error.name,
                stack: error.stack,
                properties: error.properties,
            }
            console.log(JSON.stringify({error: e}));
            // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
            throw error;
        }

        var buf = doc.getZip()
                    .generate({type: 'nodebuffer'});

                    res.writeHead(200, {
                        'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        });

        // buf is a nodejs buffer, you can either write it to a file or do anything else with it.
        // fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
        // fs.createReadStream(path.resolve(__dirname, 'output.docx'), 'binary').pipe(res);


        res.end(buf)
     });
});


// router.get('/letter-type', function(req, res) {
//     // define and prepare table parameter

//     //execute query
//     return core.ExecuteQueryWithoutParameter1(req, res, config, "usp_CRM_M_LetterType_List", true);
// });

// router.get('/letter-type-attribute', function(req, res) {
//     // define and prepare table parameter
//     const table1 = new sql.Table();
//     core.PrepareTableParameter(table1);

//     // parse json to object
//     let obj = JSON.parse(req.query.id);

//     // define parameter
//     core.AddWithParameter(table1, "IDX_M_LetterType", obj[0].IDX_M_LetterType);

//     console.log(obj);
//     //execute query
//     return core.ExecuteQueryWithParameter1(req, res, config, table1, "usp_CRM_T_LetterTypeAttribute_Load", true);
// });


// -----------------------------------------------------------------------------

module.exports = router;